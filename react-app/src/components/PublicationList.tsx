import useWorks, { Work } from '../hooks/useWorks';
import { MdLink, MdMenuBook } from 'react-icons/md';
import { useEffect, useState } from 'react';
import PublicationFilters from './PublicationFilters';

type Props = {
  orcid: string;
}

const PublicationList = ({ orcid }: Props) => {
  const { data, loading } = useWorks(orcid);
  const [selectedType, setSelectedType] = useState("all");
  const [selectedContent, setSelectedContent] = useState("");
  const [filteredData, setFilteredData] = useState<Work[] | null>(null);


  useEffect(() => {
    console.log("Filter or data updated", selectedType, selectedContent);
    const textFiltered = selectedContent ? data?.filter((work) => {
      const title = work.title.toLowerCase()
      const journal = work.journal?.toLowerCase() || "";
      if (title.includes(selectedContent) || journal.includes(selectedContent))
        return work;
    }) : data;

    const typeFiltered = selectedType && selectedType != "all" ? textFiltered?.filter(
      (work) => work.type == selectedType
    ) : textFiltered;
    
    if (typeFiltered) setFilteredData(typeFiltered);
  }, [data, selectedContent, selectedType]);

  return (
    <section>
      <h2 className="py-3 text-center bg-quaternary text-dark m-0">
        Publications
        {loading && <>
          <div className="spinner-grow ms-3" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </>}
      </h2>
      <PublicationFilters
        types={[...new Set(data?.map((work) => work.type))]} 
        onSelectText={(text) => setSelectedContent(text)}
        onSelectType={(type) => setSelectedType(type)}/>
      <div className='container-fluid p-4' style={{
        display: "grid",
        gridTemplateColumns: "repeat(auto-fill, minmax(36ch, 1fr)",
        gap: "2em"
      }}>
        {loading && <>
          <div className="card text-center">
            <div className="card-header placeholder-glow">
              <span className="placeholder">
                <div className="spinner-border" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </span>
            </div>
            <div className="card-body">
              <h5 className="card-title placeholder-glow">
                <span className="placeholder col-12"></span>
                <span className="placeholder col-8"></span>
                <span className="placeholder col-6"></span>
                <span className='placeholder col-6 btn biribil bg-dark m-3'></span>
              </h5>
              <div className='card-text placeholder-glow'>
                <div className="d-grid gap-2 col-6 mx-auto">
                  <a href="#" className="btn btn-primary disabled placeholder col"></a>
                  <a href="#" className="btn btn-primary disabled placeholder col"></a>
                </div>
              </div>
            </div>
            <div className="card-footer placeholder-glow">
              <span className="placeholder">
                <div className="spinner-border" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </span>
            </div>
          </div>
        </>}
        {filteredData?.map((work) =>
          <div className="card text-center" key={work.id}>
            <div className="card-header">
              {work.type}
            </div>
            <div className="card-body d-flex flex-column justify-content-between">
              <h5 className="card-title">
                {work.title}
                {work.journal && <>
                  <span className='bg-mgep p-2 m-4 position-relative d-block rounded text-light' title="published at">
                    {work.journal}

                    <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-tertiary">
                      <MdMenuBook />
                    </span>
                  </span>
                </>}
              </h5>
              <div className='card-text'>
                <div className="d-grid gap-2 col-11 mx-auto">

                  {work.url && !work.url.includes('doi.org') && <a href={work.url} className="btn btn-outline-secondary position-relative" title="URL">
                    {work.url}
                    <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-tertiary">
                      <MdLink />
                    </span>

                  </a>}
                  {work.doi && <a href={work.doi} className="btn btn-outline-primary position-relative" title="DOI">
                    {work.doi}
                    <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-tertiary">
                      DOI
                    </span>

                  </a>}
                  {work.issn && <a href={work.issn} className="btn btn-outline-tertiary position-relative" title="ISSN">
                    {work.issn}
                    <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-tertiary">
                      ISSN
                    </span>
                  </a>}
                </div>
              </div>
            </div>
            <div className="card-footer">
              {work.date}
            </div>
          </div>)}
      </div>
    </section>
  )
}

export default PublicationList