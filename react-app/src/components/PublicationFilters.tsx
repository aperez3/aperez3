import { MdSearch } from "react-icons/md";

type Props = {
  types: string[],
  onSelectText: (text: string) => void,
  onSelectType: (type: string) => void,
}

const PublicationFilters = ({types, onSelectText, onSelectType}:Props) => {
  return (
    
    <section id="filter" className='m-0 p-4 bg-secondary'>
    <div className='row'>
      <div className="col">
        <div className="input-group" title="Contains this text">
          <label className='input-group-text' htmlFor="textInput">
            <MdSearch />
          </label>
          <input type="text" className="form-control" id="textInput"
            onChange={(event) => onSelectText(event.target.value)}/>
        </div>
      </div>
      <div className="col">
        <div className="input-group" title='Type of the work'>
          <label className='input-group-text' htmlFor="typeSelect">Type</label>
          <select
            defaultValue="all"
            className="form-select"
            id="typeSelect"
            onChange={(event) => {
              onSelectType(event.target.value)
            }}>
            <option value="">==All==</option>
            {types?.map((type) => <option value={type} key={type}>{type}</option>)}
          </select>
        </div>
      </div>
    </div>
  </section>
  )
}

export default PublicationFilters