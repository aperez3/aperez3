import { useEffect, useState } from "react";

export type Work = {
  id: number,
  title: string,
  doi?: string,
  issn?: string,
  url?: string,
  type: string,
  date: string,
  journal?: string

}

const getPublicationDate = (date:any):string => {
  let dateStr:string = date.year?.value || "????";
  const month = date.month?.value;
  const day = date.day?.value;
  if (month) dateStr += "-"+month;
  if (day) dateStr += "-"+day;
  return dateStr;
}

const getExternalIds = (data:any):{doi?:string, issn?:string} => {
  const doiData = data?.["external-id"].filter((externalId:any) => externalId["external-id-type"] == "doi")[0];
  const issnData = data?.["external-id"].filter((externalId:any) => externalId["external-id-type"] == "issn")[0];

  return {
    doi: doiData?.["external-id-url"]?.value,
    issn: issnData?.["external-id-url"]?.value
  }
}

const listFromResponse = (res: any): Work[] => {
  const works: Work[] = res.group.map((item: any) => {
    const {doi, issn} = getExternalIds(item["work-summary"][0]["external-ids"]);
    const work: Work = {
      id:       item["work-summary"][0]["put-code"], 
      title:    item["work-summary"][0].title.title.value,
      type:     item["work-summary"][0].type,
      date:     getPublicationDate(item["work-summary"][0]["publication-date"]),
      url:      item["work-summary"][0].url?.value,
      journal:  item["work-summary"][0]["journal-title"].value,
      issn:     issn,
      doi:      doi
    }
    return work;
  });
  return works;
}

const useWorks = (orcid: string, requestConfig?: RequestInit, deps?: any[]) => {
  const [data, setData] = useState<Work[] | null>(null);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const controller = new AbortController();
    setLoading(true);

    fetch(
      "https://pub.orcid.org/v3.0/"+orcid+"/works",
      {
        signal: controller.signal, 
        ...requestConfig,
        headers: {
          ...requestConfig?.headers,
          "Accept": "application/json"
        }
      }
    )
      .then(res => res.json())
      .then((data) => {
          setError("");
          setData(listFromResponse(data));
      })
      .catch((err) => {
        if (err instanceof DOMException && err.message === "The operation was aborted. ") {
          // Do not return error if request is aborted
          setError("");
        } else {
          setError(err.message)
        }
        setData(null);
      })
      .finally(() => setLoading(false));

    return () => controller.abort();
  }, deps ? [...deps, orcid] : [orcid]);


  return { data, error, loading };
};

export default useWorks;