import PublicationList from './components/PublicationList';

function App() {
  return (<>
    <h1 className='p-4 bg-dark text-light m-0 text-end'>
      <a className='text-light text-decoration-none' href="https://www.mondragon.edu/ikerketa-transferentzia/ingeniaritza-teknologia/ikerketa-transferentzia-taldeak/-/mu-inv-mapping/investigador/alain-perez-riano">
        Alain PEREZ RIAÑO
      </a>
    </h1>
    <PublicationList orcid="0000-0002-6200-589X"/>
  </>
  )
}

export default App
